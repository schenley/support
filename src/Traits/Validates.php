<?php

namespace Schenley\Support\Traits;

use Schenley\Support\Validator;

/**
 * Part of the Support package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

trait Validates
{
	/**
	 * The Validator instance.
	 *
	 * @var \Schenley\Support\Validator
	 */
	protected $validator;

	/**
	 * Returns the Validator instance.
	 *
	 * @return \Schenley\Support\Validator
	 */
	public function getValidator()
	{
		return $this->validator;
	}

	/**
	 * Sets the Validator instance.
	 *
	 * @param  \Schenley\Support\Validator  $validator
	 * @return $this
	 */
	public function setValidator(Validator $validator)
	{
		$this->validator = $validator;
		return $this;
	}
}
